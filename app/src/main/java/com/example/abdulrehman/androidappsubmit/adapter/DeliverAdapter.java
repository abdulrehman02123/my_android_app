package com.example.abdulrehman.androidappsubmit.adapter;


import android.app.Activity;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.abdulrehman.androidappsubmit.R;
import com.example.abdulrehman.androidappsubmit.model.DeliveryModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.inloop.localmessagemanager.LocalMessageManager;

import static com.example.abdulrehman.androidappsubmit.utils.Constants.LOAD_MORE_BROADCAST;
import static com.example.abdulrehman.androidappsubmit.utils.Constants.PREVIEW_DETAIL_BROADCAST;

public class DeliverAdapter extends RecyclerView.Adapter<DeliverAdapter.MyViewHolder> {

    private Activity context;
    private ArrayList<DeliveryModel> deliveryList;
    private int offset;
    private boolean isOffline;
    private boolean progressStatus;

    public DeliverAdapter(Activity context, ArrayList<DeliveryModel> deliveryList, int offset, boolean isOffline) {
        this.context = context;
        this.deliveryList = deliveryList;
        this.offset = offset;
        this.isOffline = isOffline;

    }

    @Override
    public DeliverAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_deliveries, parent, false);
        return new DeliverAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DeliverAdapter.MyViewHolder holder, final int position) {

        if (holder.getItemViewType() == offset - 1 && !isOffline) {
            holder.progressLoad.setVisibility(View.VISIBLE);
            LocalMessageManager.getInstance().send(LOAD_MORE_BROADCAST);
            progressStatus = true;
        }

        if (!progressStatus){
            holder.progressLoad.setVisibility(View.GONE);
        }

        if (!deliveryList.get(position).getImageUrl().equalsIgnoreCase("")) {
            Glide.with(context).load(deliveryList.get(position).getImageUrl()).into(holder.circleImageView);
        }

        holder.tvDescription.setText(deliveryList.get(position).getDescription());
        holder.tvAddress.setText(deliveryList.get(position).getLocation().getAddress());

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocalMessageManager.getInstance().send(PREVIEW_DETAIL_BROADCAST, deliveryList.get(position));
            }
        });

    }

    public void update(ArrayList<DeliveryModel> deliveryList, int offset, boolean isRefresh, boolean isOffline) {
        this.offset = offset;
        this.isOffline = isOffline;
        if (!isRefresh) {
            this.deliveryList.addAll(deliveryList);
        } else {
            this.deliveryList = deliveryList;
        }
        notifyDataSetChanged();

    }

    public void setProgressStatus(boolean progressStatus){
        this.progressStatus = progressStatus;

    }

    @Override
    public int getItemCount() {
        return deliveryList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearLayout)
        LinearLayout linearLayout;
        @BindView(R.id.constraintLayout)
        ConstraintLayout constraintLayout;
        @BindView(R.id.circleImageView)
        ImageView circleImageView;
        @BindView(R.id.tvDescription)
        TextView tvDescription;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.progressLoad)
        ProgressBar progressLoad;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setTag(itemView);
        }

    }
}


package com.example.abdulrehman.androidappsubmit.deliver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.abdulrehman.androidappsubmit.R;
import com.example.abdulrehman.androidappsubmit.model.DeliveryModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PreviewDeliverDetailActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imgCircle)
    CircleImageView imgCircle;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.tvLocation)
    TextView tvLocation;

    private MapFragment mapFragment;
    private DeliveryModel deliveryData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_deliver_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.preview_toolbar_title));
        mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment));
        getData();
    }


    private void getData(){
        if (getIntent().getSerializableExtra(getString(R.string.detail_deliver_key))!= null) {
            deliveryData = (DeliveryModel) getIntent().getSerializableExtra(getString(R.string.detail_deliver_key));
            Glide.with(this).load(deliveryData.getImageUrl()).into(imgCircle);
            tvDescription.setText(deliveryData.getDescription());
            tvLocation.setText(deliveryData.getLocation().getAddress());
            showMapLocation(mapFragment, deliveryData.getLocation().getLat(), deliveryData.getLocation().getLng(), deliveryData.getLocation().getAddress());
        }
    }


    private void showMapLocation(MapFragment mapFragment, final double lat, final double lng, final String name) {

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap gMap) {
                if (gMap != null) {
                    GoogleMap googleMap;
                    googleMap = gMap;
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lat, lng))
                            .title(name));

                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 16));

                    googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {

                        }
                    });

                } else {
                    Toast.makeText(getApplicationContext(), "Map not Initialize", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

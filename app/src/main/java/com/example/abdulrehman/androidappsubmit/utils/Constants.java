package com.example.abdulrehman.androidappsubmit.utils;

import android.os.Build;

import com.example.abdulrehman.androidappsubmit.BuildConfig;

public interface Constants {


    int DELIVERY_RESPONSE_BROADCAST = 100;
    int LOAD_MORE_BROADCAST = 101;
    int REFRESH_AGAIN_BROADCAST = 102;
    int PREVIEW_DETAIL_BROADCAST = 103;

    String SHARED_PRE_KEY = "shared_pre";
    String SHARED_LOCAL_CACHE = "local_cache";

    String BASE_URL = BuildConfig.BASE_URL;
    String GET_DELIVERY_URL = BASE_URL + "/deliveries?";

}

package com.example.abdulrehman.androidappsubmit.network;

import android.content.Context;

import com.example.abdulrehman.androidappsubmit.model.DeliveryModel;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;

import eu.inloop.localmessagemanager.LocalMessageManager;

import static com.example.abdulrehman.androidappsubmit.utils.Constants.DELIVERY_RESPONSE_BROADCAST;
import static com.example.abdulrehman.androidappsubmit.utils.Constants.GET_DELIVERY_URL;
import static com.example.abdulrehman.androidappsubmit.utils.Constants.REFRESH_AGAIN_BROADCAST;

public class NetworkRequest {


    public static void getDeliveries(final Context context, int offset, int limit) {
        Ion.with(context)
                .load(GET_DELIVERY_URL + "&offset=" + offset + "&limit=" + limit)
                .as(new TypeToken<ArrayList<DeliveryModel>>() {
                })
                .withResponse().setCallback(new FutureCallback<Response<ArrayList<DeliveryModel>>>() {
            @Override
            public void onCompleted(Exception e, Response<ArrayList<DeliveryModel>> result) {
                if (result != null) {

                    if (result.getHeaders().code() == 200 || result.getHeaders().code() == 201){
                        LocalMessageManager.getInstance().send(DELIVERY_RESPONSE_BROADCAST, result.getResult());
                    }else if (result.getHeaders().code() == 500 || result.getHeaders().code() == 400){
                        LocalMessageManager.getInstance().send(REFRESH_AGAIN_BROADCAST, null);
                    }
                    else {
                        LocalMessageManager.getInstance().send(DELIVERY_RESPONSE_BROADCAST, null);
                    }
                } else {
                    LocalMessageManager.getInstance().send(REFRESH_AGAIN_BROADCAST, null);
                }
            }
        });

    }

}

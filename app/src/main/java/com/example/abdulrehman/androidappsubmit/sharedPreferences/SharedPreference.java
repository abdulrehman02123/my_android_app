package com.example.abdulrehman.androidappsubmit.sharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.abdulrehman.androidappsubmit.model.DeliveryModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.example.abdulrehman.androidappsubmit.utils.Constants.SHARED_LOCAL_CACHE;
import static com.example.abdulrehman.androidappsubmit.utils.Constants.SHARED_PRE_KEY;

public class SharedPreference {

    private static SharedPreference sharedPreference;
    private static final String DATE_FORMATE = "yyyy-MM-dd kk:mm:ss";
    private  String EMPTY_STRING = " ";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    private SharedPreference(Context  context) {
        preferences = context.getSharedPreferences(SHARED_PRE_KEY, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public static SharedPreference getInstance(Context context){
        if (sharedPreference == null){
            sharedPreference = new SharedPreference(context);
            return sharedPreference;
        }else {
            return sharedPreference;
        }
    }

    public ArrayList<DeliveryModel> getDeliveryList() {
        GsonBuilder builder = new GsonBuilder();
        Gson mGson = builder.create();
        return mGson.fromJson(preferences.getString(SHARED_LOCAL_CACHE, EMPTY_STRING),
                new TypeToken<ArrayList<DeliveryModel>>() {
        }.getType());

    }

    public void setDeliveryList(ArrayList<DeliveryModel> deliveryList) {
        Gson gson = new GsonBuilder()
                .setDateFormat(DATE_FORMATE)
                .create();
        String json = gson.toJson(deliveryList);
        editor.putString(SHARED_LOCAL_CACHE, json);
        editor.commit();
    }
}

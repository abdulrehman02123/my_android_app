package com.example.abdulrehman.androidappsubmit.deliver;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abdulrehman.androidappsubmit.R;
import com.example.abdulrehman.androidappsubmit.adapter.DeliverAdapter;
import com.example.abdulrehman.androidappsubmit.model.DeliveryModel;
import com.example.abdulrehman.androidappsubmit.sharedPreferences.SharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import eu.inloop.localmessagemanager.LocalMessage;
import eu.inloop.localmessagemanager.LocalMessageCallback;
import eu.inloop.localmessagemanager.LocalMessageManager;

import static com.example.abdulrehman.androidappsubmit.network.NetworkRequest.getDeliveries;
import static com.example.abdulrehman.androidappsubmit.utils.Constants.DELIVERY_RESPONSE_BROADCAST;
import static com.example.abdulrehman.androidappsubmit.utils.Constants.LOAD_MORE_BROADCAST;
import static com.example.abdulrehman.androidappsubmit.utils.Constants.PREVIEW_DETAIL_BROADCAST;
import static com.example.abdulrehman.androidappsubmit.utils.Constants.REFRESH_AGAIN_BROADCAST;
import static com.example.abdulrehman.androidappsubmit.utils.Utilities.isNetworkAvailable;
import static com.example.abdulrehman.androidappsubmit.utils.Utilities.isNotNull;

public class DeliverActivity extends AppCompatActivity implements LocalMessageCallback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.tvRefresh)
    TextView tvRefresh;
    @BindView(R.id.tvRefreshAgain)
    TextView tvRefreshAgain;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    private DeliverAdapter deliveryAdapter;
    private int size = 20;
    private int offset = 0;
    private int limit = size;
    private boolean isRefresh = false;
    private boolean isOffline = false;
    private ArrayList<DeliveryModel> localList = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deliver);
        ButterKnife.bind(this);
        LocalMessageManager.getInstance().addListener(this);
        toolbar.setTitle(getString(R.string.toolbar_title));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        hideView();
        swipeToRefresh();
        sendNetworkRequest();

    }


    private void sendNetworkRequest() {
        if (isNetworkAvailable(this)) {
            getDeliveries(this, offset, limit);
            isOffline = false;
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_not_available), Toast.LENGTH_SHORT).show();
            isOffline = true;
            swipeRefresh.setRefreshing(false);
            ArrayList<DeliveryModel> deliveryModels = SharedPreference.getInstance(this).getDeliveryList();
            setRvAdapter(deliveryModels);
            showView();
        }
    }

    @Override
    public void handleMessage(@NonNull LocalMessage localMessage) {
        if (DELIVERY_RESPONSE_BROADCAST == localMessage.getId()) {
            ArrayList<DeliveryModel> deliveryList = (ArrayList<DeliveryModel>) localMessage.getObject();
            showView();
            if (isNotNull(deliveryAdapter)) {
                deliveryAdapter.setProgressStatus(false);
            }
            populateData(deliveryList);

        } else if (localMessage.getId() == LOAD_MORE_BROADCAST) {
            isRefresh = false;
            offset = offset + size;
            sendNetworkRequest();

        } else if (REFRESH_AGAIN_BROADCAST == localMessage.getId()) {
            tvRefreshAgain.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
        else if (PREVIEW_DETAIL_BROADCAST == localMessage.getId()){
            DeliveryModel deliverDetail = (DeliveryModel) localMessage.getObject();
            Intent intent = new Intent(this, PreviewDeliverDetailActivity.class);
            intent.putExtra(getString(R.string.detail_deliver_key),deliverDetail);
            startActivity(intent);
        }
    }


    private void populateData(ArrayList<DeliveryModel> deliveryList) {
        if (isNotNull(deliveryList)) {
            if (!deliveryList.isEmpty()) {
                setRvAdapter(deliveryList);
                localList.addAll(deliveryList);
            }
        } else {
            setRvAdapter(new ArrayList<DeliveryModel>());
            tvRefresh.setVisibility(View.VISIBLE);
        }

    }


    private void setRvAdapter(ArrayList<DeliveryModel> deliveryList) {
        if (deliveryAdapter == null) {
            deliveryAdapter = new DeliverAdapter(this, deliveryList, (offset + size), isOffline);
            recyclerView.setAdapter(deliveryAdapter);
        } else {
            deliveryAdapter.update(deliveryList, (offset + size), isRefresh, isOffline);
        }
    }


    private void hideView() {
        swipeRefresh.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void showView() {
        swipeRefresh.setVisibility(View.VISIBLE);
        swipeRefresh.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        tvRefreshAgain.setVisibility(View.GONE);
    }

    private void swipeToRefresh() {
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                offset = 0;
                isRefresh = true;
                localList.clear();
                sendNetworkRequest();

            }
        });
    }


    @OnClick(R.id.tvRefreshAgain)
    void refreshAgainWhenNull() {
        tvRefreshAgain.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        sendNetworkRequest();
    }

    @OnClick(R.id.tvRefresh)
    void refreshDuringLoadMore() {
        tvRefresh.setVisibility(View.GONE);
        sendNetworkRequest();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreference.getInstance(this).setDeliveryList(localList);
        LocalMessageManager.getInstance().removeListener(this);
    }
}
